\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {UKenglish}
\contentsline {section}{\numberline {1}Introduction}{3}{section.1}
\contentsline {section}{\numberline {2}Data and Simulated Samples}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Data}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}MC samples}{3}{subsection.2.2}
\contentsline {section}{\numberline {3}Event selection}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}Derivations}{4}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Object and event selection}{5}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Data to MC comparison}{5}{subsection.3.3}
\contentsline {section}{\numberline {4}Template method}{7}{section.4}
\contentsline {section}{\numberline {5}Results}{8}{section.5}
\contentsline {subsection}{\numberline {5.1}Inclusive photon energy scale factor}{8}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Differential photon energy scale factor}{9}{subsection.5.2}
\contentsline {part}{Appendices}{14}{part*.22}
\contentsline {section}{\numberline {A}Appendix}{14}{appendix.A}
\contentsline {subsection}{\numberline {A.1}Templates and $\chi ^{2}$ curves}{14}{subsection.A.1}
